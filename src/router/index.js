import Vue from 'vue'
import Router from 'vue-router'
import ListCats from '@/components/organization/listCats'
import ResultVote from '@/components/organization/resultVote'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '*',
      name: 'ListCat',
      component: ListCats
    },
    {
      path: '/',
      name: 'ListCat',
      component: ListCats
    },
    {
      path: '/result',
      name: 'ResultVote',
      component: ResultVote
    }
  ]
})
